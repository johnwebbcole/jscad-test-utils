import Debug from 'debug';
import fs from 'fs';
import looksSame from 'looks-same';
import path from 'path';
import csgReglRenderer from './csgReglRenderer';
const debug = Debug('jscad-test-utils:csgImageSnapshot');

/**
 * Image testing with a snapshot image for CSG objects.  Uses [looks-same](https://www.npmjs.com/package/looks-same)
 * to comare the images.  If there is a differnece,
 * the new `temp.png` file anda `diff.png` file are created.
 * @function csgImageSnapshot
 * @param  {string} testpathname       The test path name to.
 * @param  {string} title  The title of the test.
 * @param  {CSG} data    CSG data to render
 * @param  {object} [options] Options to pass to the ReglRenderer.
 * @param  {object} [options.camera] Options to pass to the ReglRenderer camera.
 * @param  {number[]} [options.camera.position=[50, -50, 50]] The position of the camera while rendering.
 * @param  {object} [options.gl] Options to pass to the ReglRenderer glRenderer.
 * @param  {number} [options.gl.width=1024] The width of the rendered image.
 * @param  {number} [options.gl.height=768] The height of the rendered image.
 * @return {Promise} A promise that resolves true if the rendered images matches the snapshot, false if there is a difference.
 *
 * @example
 * In the test, create a CSG object then call `csgImageSnapshot` with
 * the test object and the CSG data.
 *
 * Creates a snapshot of the sphere.
 * ![snapshot image](jsdoc2md/unit-test.png)
 *
 * ```javascript
 * test('create a sphere', async t => {
 *   var sphere2 = CSG.sphere({
 *     center: [0, 0, 0],
 *     radius: 11, // must be scalar
 *     resolution: 128
 *   });
 *
 *
 *   var result = await csgImageSnapshot(t, sphere2);
 *   t.false(result);
 * }
 *   ```
 *
 * If the test fails, a `diff.png` file is created showing the differneces.
 *
 * ![failed snapshot](jsdoc2md/failed-unit-test.png)
 */
export default async function csgImageSnapshot(testpathname, title, data, options = {}) {
  debug('csgImageSnapshot', testpathname, title);
  debug('options', options);

  const imageDir = `${path.dirname(testpathname)}/images`;
  const pngTempFileName = `${imageDir}/${title}.temp.png`;
  const pngSnapFileName = `${imageDir}/${title}.snap.png`;
  const pngDiffFileName = `${imageDir}/${title}.diff.png`;

  if (fs.existsSync(pngSnapFileName)) {
    debug('options1', options);
    csgReglRenderer(data, pngTempFileName, options);

    const isSame = await looksSame(pngSnapFileName, pngTempFileName, { strict: true })
    if (!isSame.equal) {
      await looksSame.createDiff(
        {
          reference: pngSnapFileName,
          current: pngTempFileName,
          diff: pngDiffFileName,
          highlightColor: '#ff0000', // color to highlight the differences
          strict: true
        });
      return isSame.equal;

    } else {
      if (fs.existsSync(pngTempFileName)) fs.unlinkSync(pngTempFileName);
      if (fs.existsSync(pngDiffFileName)) fs.unlinkSync(pngDiffFileName);
      return isSame.equal;
    }
  } else {
    if (fs.existsSync(pngTempFileName)) fs.unlinkSync(pngTempFileName);
    if (fs.existsSync(pngDiffFileName)) fs.unlinkSync(pngDiffFileName);
    /**
     * Write the missing snapshot file.
     */
    debug('options2', options);
    csgReglRenderer(data, pngSnapFileName, options);
    return true;
  }
}
