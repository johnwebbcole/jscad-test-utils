const { writeContextToFile } = require('@jwc/jscad2-img-utils');
const {
  prepareRender,
  drawCommands,
  cameras,
  entitiesFromSolids
} = require('@jwc/jscad2-regl-renderer'); // replace this with the correct import
var fs = require('fs');
import Debug from 'debug';
const debug = Debug('jscad-test-utils:csgReglRenderer');

/**
 * GL render options
 * @typedef glRenderOptions
 * @param {number} width - The width of the rendered image (default 1024).
 * @param {number} height - The height of the rendered image (default 768).
 */

/**
 * Camera render options
 * @typedef cameraRenderOptions
 * @param {Array} position - The position of the camera while rendering (default [50, -50, 50]).
 */

/**
 * Options for the csgReglRenderer
 * @typedef RenderOptions
 * @param {cameraRenderOptions} camera
 * @param {glRenderOptions} gl
 */

/**
 * Renders a JsCad CSG data object into a png file
 * @function csgReglRenderer
 * @param  {CSG} data     A JsCad CSG object.
 * @param  {String} filename The filename to render the png image.
 * @param  {object} [options] Options to pass to the ReglRenderer.
 * @param  {object} [options.camera] Options to pass to the ReglRenderer camera.
 * @param  {number[]} [options.camera.position=[50, -50, 50]] The position of the camera while rendering.
 * @param  {object} [options.gl] Options to pass to the ReglRenderer glRenderer.
 * @param  {number} [options.gl.width=1024] The width of the rendered image.
 * @param  {number} [options.gl.height=768] The height of the rendered image.
 *
 * @example
 * Render a CSG sphere to the file `test.png`.
 *
 * ```javascript
 * var sphere = CSG.sphere({
 * center: [0, 0, 0],
 * radius: 2, // must be scalar
 * resolution: 128
 * });
 *
 * if (fs.existsSync("test.png")) fs.unlinkSync("test.png");
 * csgReglRenderer(sphere, "test.png");
 * ```
 */
export default function csgReglRenderer(data, filename, options = {}) {
  debug('options1', options);
  options = Object.assign(
    {
      camera: {
        position: [50, -50, 50]
      },
      gl: {
        width: 1024,
        height: 768
      }
    },
    options
  );

  debug('csgReglRenderer', filename);
  debug('options', options);

  /**
   * Delete the file if it exists.
   */
  if (fs.existsSync(filename)) fs.unlinkSync(filename);

  const { width, height } = options.gl;
  // create webgl context
  const gl = require('gl')(width, height);

  // process entities and inject extras
  const solids = entitiesFromSolids({}, data);

  // prepare the camera
  const perspectiveCamera = cameras.perspective;
  const camera = Object.assign({}, perspectiveCamera.defaults, options.camera);
  perspectiveCamera.setProjection(camera, camera, { width, height });
  perspectiveCamera.update(camera, camera);

  const renderOptions = {
    glOptions: { gl },
    camera,
    drawCommands: {
      // draw commands bootstrap themselves the first time they are run
      drawGrid: drawCommands.drawGrid, // require('./src/rendering/drawGrid/index.js'),
      drawAxis: drawCommands.drawAxis, // require('./src/rendering/drawAxis'),
      drawMesh: drawCommands.drawMesh // require('./src/rendering/drawMesh/index.js')
    },
    rendering: {
      background: [1, 1, 1, 1],
      meshColor: [1, 0.5, 0.5, 1], // use as default face color for csgs, color for cags
      lightColor: [1, 1, 1, 1], // note: for now there is a single preset light, not an entity
      lightDirection: [0.2, 0.2, 1],
      lightPosition: [100, 200, 100],
      ambientLightAmount: 0.3,
      diffuseLightAmount: 0.89,
      specularLightAmount: 0.16,
      materialShininess: 8.0
    },
    // next few are for solids / csg/ cags specifically
    overrideOriginalColors: false, // for csg/cag conversion: do not use the original (csg) color, use meshColor instead
    smoothNormals: true,

    // data
    entities: [
      {
        // grid data
        // the choice of what draw command to use is also data based
        visuals: {
          drawCmd: 'drawGrid',
          show: true,
          color: [0, 0, 0, 0.1],
          subColor: [0, 0, 1, 0.1],
          fadeOut: true,
          transparent: true
        },
        size: [100, 100],
        ticks: [100, 10]
      },
      {
        visuals: {
          drawCmd: 'drawAxis',
          show: true
        }
      },
      ...solids
    ]
  };

  // prepare
  const render = prepareRender(renderOptions);
  // do the actual render
  render(renderOptions);
  // output to file
  writeContextToFile(gl, width, height, 4, filename);
}
