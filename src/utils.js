import Debug from 'debug';
import looksSame from 'looks-same';
const debug = Debug('jscad-test-utils:utils');

export function createDiff(t, reference, current, diff) {
  return looksSame.createDiff(
    {
      reference,
      current,
      diff,
      highlightColor: '#ff0000', // color to highlight the differences
      strict: true
    }
  );
}

export function compareImages(reference, current) {
  debug('compareImages', reference, current);
  return looksSame(reference, current, { strict: true });
}
