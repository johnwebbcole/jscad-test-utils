![hero image](./test.png)


[![view on
npm](http://img.shields.io/npm/v/@jwc/jscad-test-utils.svg)](https://www.npmjs.org/package/@jwc/jscad-test-utils)
[![npm module
downloads](http://img.shields.io/npm/dt/@jwc/jscad-test-utils.svg)](https://www.npmjs.org/package/@jwc/jscad-test-utils)
![Dependents](http://img.shields.io/librariesio/dependents/npm/@jwc/jscad-test-utils)

![Twitter Follow](https://img.shields.io/twitter/follow/johnwebbcole?label=Follow&style=social)

# jscad-test-utils

This packages has two utilities to help testing [JsCad] objects using image snapshots with [AVA].

## csgReglRenderer

Based on [@jscad-regl-rendere](https://github.com/jscad/OpenJSCAD.org/tree/V2/packages/utils/regl-renderer) the
csgReglRenderer provides an easy way to generate a png file from a CSG object.

```javascript
var sphere = CSG.sphere({
center: [0, 0, 0],
radius: 2, // must be scalar
resolution: 128
});

if (fs.existsSync("test.png")) fs.unlinkSync("test.png");
csgReglRenderer(sphere, "test.png");
```

## API

## Functions

<dl>
<dt><a href="#csgImageSnapshot">csgImageSnapshot(testpathname, title, data, [options])</a> ⇒ <code>Promise</code></dt>
<dd><p>Image testing with a snapshot image for CSG objects.  Uses <a href="https://www.npmjs.com/package/looks-same">looks-same</a>
to comare the images.  If there is a differnece,
the new <code>temp.png</code> file anda <code>diff.png</code> file are created.</p>
</dd>
<dt><a href="#csgReglRenderer">csgReglRenderer(data, filename, [options])</a></dt>
<dd><p>Renders a JsCad CSG data object into a png file</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#glRenderOptions">glRenderOptions</a></dt>
<dd><p>GL render options</p>
</dd>
<dt><a href="#cameraRenderOptions">cameraRenderOptions</a></dt>
<dd><p>Camera render options</p>
</dd>
<dt><a href="#RenderOptions">RenderOptions</a></dt>
<dd><p>Options for the csgReglRenderer</p>
</dd>
</dl>

<a name="csgImageSnapshot"></a>

## csgImageSnapshot(testpathname, title, data, [options]) ⇒ <code>Promise</code>
Image testing with a snapshot image for CSG objects.  Uses [looks-same](https://www.npmjs.com/package/looks-same)
to comare the images.  If there is a differnece,
the new `temp.png` file anda `diff.png` file are created.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise that resolves true if the rendered images matches the snapshot, false if there is a difference.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| testpathname | <code>string</code> |  | The test path name to. |
| title | <code>string</code> |  | The title of the test. |
| data | <code>CSG</code> |  | CSG data to render |
| [options] | <code>object</code> |  | Options to pass to the ReglRenderer. |
| [options.camera] | <code>object</code> |  | Options to pass to the ReglRenderer camera. |
| [options.camera.position] | <code>Array.&lt;number&gt;</code> | <code>[50, -50, 50]</code> | The position of the camera while rendering. |
| [options.gl] | <code>object</code> |  | Options to pass to the ReglRenderer glRenderer. |
| [options.gl.width] | <code>number</code> | <code>1024</code> | The width of the rendered image. |
| [options.gl.height] | <code>number</code> | <code>768</code> | The height of the rendered image. |

**Example**  
In the test, create a CSG object then call `csgImageSnapshot` with
the test object and the CSG data.

Creates a snapshot of the sphere.
![snapshot image](jsdoc2md/unit-test.png)

```javascript
test('create a sphere', async t => {
  var sphere2 = CSG.sphere({
    center: [0, 0, 0],
    radius: 11, // must be scalar
    resolution: 128
  });


  var result = await csgImageSnapshot(t, sphere2);
  t.false(result);
}
  ```

If the test fails, a `diff.png` file is created showing the differneces.

![failed snapshot](jsdoc2md/failed-unit-test.png)
<a name="csgReglRenderer"></a>

## csgReglRenderer(data, filename, [options])
Renders a JsCad CSG data object into a png file

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| data | <code>CSG</code> |  | A JsCad CSG object. |
| filename | <code>String</code> |  | The filename to render the png image. |
| [options] | <code>object</code> |  | Options to pass to the ReglRenderer. |
| [options.camera] | <code>object</code> |  | Options to pass to the ReglRenderer camera. |
| [options.camera.position] | <code>Array.&lt;number&gt;</code> | <code>[50, -50, 50]</code> | The position of the camera while rendering. |
| [options.gl] | <code>object</code> |  | Options to pass to the ReglRenderer glRenderer. |
| [options.gl.width] | <code>number</code> | <code>1024</code> | The width of the rendered image. |
| [options.gl.height] | <code>number</code> | <code>768</code> | The height of the rendered image. |

**Example**  
Render a CSG sphere to the file `test.png`.

```javascript
var sphere = CSG.sphere({
center: [0, 0, 0],
radius: 2, // must be scalar
resolution: 128
});

if (fs.existsSync("test.png")) fs.unlinkSync("test.png");
csgReglRenderer(sphere, "test.png");
```
<a name="glRenderOptions"></a>

## glRenderOptions
GL render options

**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| width | <code>number</code> | The width of the rendered image (default 1024). |
| height | <code>number</code> | The height of the rendered image (default 768). |

<a name="cameraRenderOptions"></a>

## cameraRenderOptions
Camera render options

**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| position | <code>Array</code> | The position of the camera while rendering (default [50, -50, 50]). |

<a name="RenderOptions"></a>

## RenderOptions
Options for the csgReglRenderer

**Kind**: global typedef  

| Param | Type |
| --- | --- |
| camera | [<code>cameraRenderOptions</code>](#cameraRenderOptions) | 
| gl | [<code>glRenderOptions</code>](#glRenderOptions) | 

