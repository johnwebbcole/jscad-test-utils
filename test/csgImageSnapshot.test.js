import jsCadCSG from '@jscad/csg';
import Debug from 'debug';
import fs from 'fs';
import path from 'path';
import { expect, test } from 'vitest';
import { csgImageSnapshot, csgReglRenderer } from '../src';
import { compareImages } from '../src/utils';
const debug = Debug('jscad-test-utils:csgImageSnapshot-test');
const { CSG, CAG } = jsCadCSG;

// test.after.always.cb('wait for logging', t => {
//   setTimeout(t.end, 250);
// });

test.sequential('csgImageSnapshot - creates a snapshot if none exist', async t => {
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });

  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  var testimagename = path.resolve(imageDir, `${t.task.name}.snap.png`);

  /**
   * Delete the snapshot if it exists
   */
  if (fs.existsSync(testimagename)) fs.unlinkSync(testimagename);
  expect(fs.existsSync(testimagename)).toBe(false);

  var result = await csgImageSnapshot(t.task.file?.filepath, t.task.name, sphere);
  expect(result).toBe(true);

  /**
   * Confirm the snapshot exists now
   */
  expect(fs.existsSync(testimagename)).toBe(true);
});

test.sequential('csgImageSnapshot - creates a snapshot if none exist with options', async t => {
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });

  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  var testimagename = path.resolve(imageDir, `${t.task.name}.snap.png`);
  var testimagenameReference = path.resolve(
    imageDir,
    `${t.task.name}.reference.png`
  );

  /**
   * Delete the snapshot if it exists
   */
  if (fs.existsSync(testimagename)) fs.unlinkSync(testimagename);
  expect(fs.existsSync(testimagename)).toBe(false);

  var result = await csgImageSnapshot(t.task.file?.filepath, t.task.name, sphere, {
    camera: {
      position: [100, -100, 100]
    }
  });
  expect(result).toBe(true);

  /**
   * Confirm the snapshot exists now
   */
  expect(fs.existsSync(testimagename)).toBe(true);

  if (fs.existsSync(testimagenameReference)) {
    var refResult = await compareImages(testimagenameReference, testimagename);
    if (refResult) {
      fs.unlinkSync(testimagename);
    }
  } else {
    fs.renameSync(testimagename, testimagenameReference);
  }
});

test.sequential('csgImageSnapshot - compares a snapshot', async t => {
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });
  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  var testimagename = path.resolve(imageDir, `${t.task.name}.snap.png`);

  if (!fs.existsSync(testimagename)) csgReglRenderer(sphere, testimagename);
  expect(fs.existsSync(testimagename)).toBe(true);

  var result = await csgImageSnapshot(t.task.file?.filepath, t.task.name, sphere);
  expect(result).toBe(true);
  expect(fs.existsSync(testimagename)).toBe(true);
});

test.sequential('csgImageSnapshot - returns false when snapshots do not match', async t => {
  var sphere1 = CSG.sphere({
    center: [0, 0, 0],
    radius: 10, // must be scalar
    resolution: 128
  });

  var sphere2 = CSG.sphere({
    center: [0, 0, 0],
    radius: 11, // must be scalar
    resolution: 128
  });
  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  var testimagename = path.resolve(imageDir, `${t.task.name}.snap.png`);

  /**
   * Create a snapshot with sphere1
   */
  if (fs.existsSync(testimagename)) fs.unlinkSync(testimagename);
  csgReglRenderer(sphere1, testimagename);
  expect(fs.existsSync(testimagename)).toBe(true);

  /**
   * Test snapshot with sphere2
   */
  var result = await csgImageSnapshot(t.task.file?.filepath, t.task.name, sphere2);
  expect(result).toBe(false);
  expect(fs.existsSync(testimagename)).toBe(true);
  expect(fs.existsSync(path.resolve(imageDir, `${t.task.name}.temp.png`))).toBe(true);
  expect(fs.existsSync(path.resolve(imageDir, `${t.task.name}.diff.png`))).toBe(true);
});

test.sequential('csgImageSnapshot - snapshot can change the camera position', async t => {
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });
  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  var testimagename = path.resolve(imageDir, `${t.task.name}.snap.png`);
  var testimagenameTemp = path.resolve(imageDir, `${t.task.name}.temp.png`);
  var testimagenameDiff = path.resolve(imageDir, `${t.task.name}.diff.png`);
  var testimagenameDiffSnap = path.resolve(imageDir, `${t.task.name}.diffsnap.png`);

  function cleanupIamges() {
    if (fs.existsSync(testimagenameTemp)) fs.unlinkSync(testimagenameTemp);
    if (fs.existsSync(testimagenameDiff)) fs.unlinkSync(testimagenameDiff);
  }
  cleanupIamges();

  if (!fs.existsSync(testimagename)) csgReglRenderer(sphere, testimagename);
  expect(fs.existsSync(testimagename)).toBe(true);

  var result = await csgImageSnapshot(t.task.file?.filepath, t.task.name, sphere, {
    camera: {
      position: [100, -100, 100]
    }
  });
  expect(result).toBe(false);
  expect(fs.existsSync(testimagename)).toBe(true);

  /**
   * Compare the diff file to the diff snapshot
   * making sure they are the same.
   */
  if (fs.existsSync(testimagenameDiffSnap)) {
    var diffResult = await compareImages(
      testimagenameDiffSnap,
      testimagenameDiff
    );
    expect(diffResult.equal).toBe(true);
  } else {
    fs.renameSync(testimagenameDiff, testimagenameDiffSnap);
  }

  if (diffResult.equal) {
    cleanupIamges();
  }
});
