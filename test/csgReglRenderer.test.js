import fs from 'fs';
import path from 'path';
import { expect, test } from 'vitest';
import { csgReglRenderer } from '../src';
import { compareImages, createDiff } from '../src/utils';

import jsCadCSG from '@jscad/csg';
// import scadApi from '@jscad/scad-api';
const { CSG, CAG } = jsCadCSG;

// global.CSG = CSG;
// global.CAG = CAG;
// global.jsCadCSG = jsCadCSG;
// import Debug from 'debug';
// const debug = Debug('jscad-test-utils:csgReglRenderer-test');

// const imageDir = `${path.dirname(test.meta.file)}/images`;

// test.after.always.cb('wait for logging', t => {
//   setTimeout(t.end, 250);
// });

test.sequential('csgReglRenderer - render a csg object', async t => {
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });

  if (fs.existsSync('test.png')) fs.unlinkSync('test.png');
  await csgReglRenderer(sphere, 'test.png');
  expect(fs.existsSync('test.png')).toBe(true);
});

test.sequential('csgReglRenderer - change the camera position', async t => {
  const imageDir = `${path.dirname(t.task.file.filepath)}/images`;
  // console.log("task", t.task);
  var sphere = CSG.sphere({
    center: [0, 0, 0],
    radius: 2, // must be scalar
    resolution: 128
  });

  var testimagename = path.resolve(imageDir, `${t.task.name}-1.temp.png`);
  var testimagename2 = path.resolve(imageDir, `${t.task.name}-2.temp.png`);
  var testimagenameDiff = path.resolve(imageDir, `${t.task.name}-diff.diff.png`);
  var testimagenameSnap = path.resolve(imageDir, `${t.task.name}.snap.png`);

  function cleanupImages() {
    if (fs.existsSync(testimagename)) fs.unlinkSync(testimagename);
    if (fs.existsSync(testimagename2)) fs.unlinkSync(testimagename2);
    if (fs.existsSync(testimagenameDiff)) fs.unlinkSync(testimagenameDiff);
  }
  cleanupImages();

  /**
   * Render at the default position.
   */
  await csgReglRenderer(sphere, testimagename, {
    camera: {
      position: [50, -50, 50]
    }
  });

  /**
   * Render with a different camera position.
   */
  await csgReglRenderer(sphere, testimagename2, {
    camera: {
      position: [100, -100, 100]
    }
  });

  expect(fs.existsSync(testimagename)).toBe(true);
  expect(fs.existsSync(testimagename2)).toBe(true);

  await createDiff(t, testimagename, testimagename2, testimagenameDiff);

  var cleanup = true;

  if (fs.existsSync(testimagenameSnap)) {
    var result = await compareImages(testimagenameSnap, testimagenameDiff);
    expect(result.equal).toBe(true);
    cleanup = result.equal;
  } else {
    // copy the diff to the snap image.
    fs.renameSync(testimagenameDiff, testimagenameSnap);
  }

  if (cleanup) {
    cleanupImages();
  }
  console.log("B")

});
